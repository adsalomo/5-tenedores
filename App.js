// Error timer
//import SolucionTimer from "./app/lib/SolucionTimer";

import React from "react";
import { StyleSheet, View } from "react-native";

// FireBase
import fireBaseApp from "./app/utils/FireBase";

// Navigation
import UserNavigation from "./app/navigations/User";

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <UserNavigation></UserNavigation>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
