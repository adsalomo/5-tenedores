import * as fireBase from "firebase";

export const uploadImage = async (uri, nameImage, folder) => {
  return await fetch(uri)
    .then(async result => {
      elStorage = fireBase
        .storage()
        .ref()
        .child(folder + "//" + nameImage);

      await elStorage.put(result._bodyBlob);

      return await elStorage
        .getDownloadURL()
        .then(resolve => {
          return resolve;
        })
        .catch(erro => {
          console.log("Error obtener imagen: ", erro);
          throw erro;
        });
    })
    .catch(erro => {
      console.log("Error subir imagen: ", erro);
      throw erro;
    });
};
