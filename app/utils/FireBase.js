import fireBase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAOOR9p2ydlewx4_1KaQwyQ_5KR5bhBFPQ",
  authDomain: "tenedores-57bc8.firebaseapp.com",
  databaseURL: "https://tenedores-57bc8.firebaseio.com",
  projectId: "tenedores-57bc8",
  storageBucket: "tenedores-57bc8.appspot.com",
  messagingSenderId: "838236361692",
  appId: "1:838236361692:web:3b53132a3fa847c57b3519"
};

export const fireBaseApp = fireBase.initializeApp(firebaseConfig);
