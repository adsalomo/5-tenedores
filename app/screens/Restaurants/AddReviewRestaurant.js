import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import { AirbnbRating, Button, Overlay } from "react-native-elements";
import Toast, { DURATION } from "react-native-easy-toast";

import t from "tcomb-form-native";
const Form = t.form.Form;
import {
  AddReviewRestaurantStruct,
  AddReviewRestaurantOptions
} from "../../forms/AddReviewRestaurant";

import { fireBaseApp } from "../../utils/FireBase";
import fireBase from "firebase/app";
import "firebase/firestore";
const db = fireBase.firestore(fireBaseApp);

export default class AddReviewRestaurant extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    };
  }

  sendReview = () => {
    this.setState({
      isLoading: true
    });

    const ratingValue = this.refs.rating.state.position;

    if (ratingValue === 0) {
      this.refs.toast.show("Tienes que puntuar el restaurante.", 1500);
      this.setState({
        isLoading: false
      });
    } else {
      const validate = this.refs.addReviewRestaurantForm.getValue();
      if (!validate) {
        this.refs.toast.show("Completa el formulario.", 1500);
        this.setState({
          isLoading: false
        });
      } else {
        const user = fireBase.auth().currentUser;
        const data = {
          id: user.uid,
          avatarUser: user.photoURL,
          idRestaurant: this.props.navigation.state.params.id,
          title: validate.title,
          review: validate.review,
          rating: ratingValue,
          createAt: new Date()
        };

        db.collection("reviews")
          .add(data)
          .then(() => {
            const restaurantRef = db
              .collection("restaurants")
              .doc(this.props.navigation.state.params.id);

            restaurantRef
              .get()
              .then(response => {
                const restaurantData = response.data();
                const ratingTotal = restaurantData.ratingTotal + ratingValue;
                const quantityVoting = restaurantData.quantityVoting + 1;
                const rating = ratingTotal / quantityVoting;

                restaurantRef
                  .update({ rating, ratingTotal, quantityVoting })
                  .then(() => {
                    this.refs.toast.show(
                      "Review enviada correctamente.",
                      50,
                      () => {
                        this.props.navigation.state.params.loadReviews();
                        this.props.navigation.goBack();
                      }
                    );
                  })
                  .catch(erro => {
                    console.log("Error actualizando rating.", erro);
                    this.refs.toast.show("Error actualizando rating.", 1500);
                  });
              })
              .catch(erro => {
                console.log("Error consultando restaurante.", erro);
                this.refs.toast.show("Error consultando restaurant.", 1500);
              });
          })
          .catch(erro => {
            console.log("Error enviando review.", erro);
            this.refs.toast.show("Error enviando review.", 1500);
          })
          .finally(() => {
            this.setState({
              isLoading: false
            });
          });
      }
    }
  };

  render() {
    const { isLoading } = this.state;

    return (
      <KeyboardAvoidingView style={styles.viewBody} behavior="padding">
        <View styles={styles.viewBody}>
          <View style={styles.viewRating}>
            <AirbnbRating
              ref="rating"
              count={5}
              reviews={[
                "Pésimo",
                "Deficiente",
                "Normal",
                "Muy Bueno",
                "Excelente"
              ]}
              defaultRating={0}
              size={35}
            ></AirbnbRating>
          </View>

          <View style={styles.formReview}>
            <Form
              ref="addReviewRestaurantForm"
              type={AddReviewRestaurantStruct}
              options={AddReviewRestaurantOptions}
            ></Form>
          </View>

          <View style={styles.viewSendReview}>
            <Button
              title="Enviar"
              buttonStyle={styles.btnSendReview}
              onPress={() => this.sendReview()}
            ></Button>
          </View>

          <Overlay
            style={{ padding: 20 }}
            isVisible={isLoading}
            width="auto"
            height="auto"
          >
            <View>
              <Text
                style={{ color: "#00a680", marginBottom: 20, fontSize: 16 }}
              >
                Enviando Review
              </Text>
              <ActivityIndicator
                size="large"
                color="#00a680"
              ></ActivityIndicator>
            </View>
          </Overlay>

          <Toast
            ref="toast"
            position="bottom"
            positionValue={250}
            fadeInDuration={1000}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{ color: "#fff" }}
          ></Toast>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1
  },
  viewRating: {
    height: 110,
    backgroundColor: "#f2f2f2"
  },
  formReview: {
    marginTop: 40,
    margin: 10
  },
  viewSendReview: {
    justifyContent: "flex-end",
    marginBottom: 30
  },
  btnSendReview: {
    backgroundColor: "#00a680",
    marginLeft: 20,
    marginRight: 20
  }
});
