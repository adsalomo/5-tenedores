import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  ActivityIndicator,
  KeyboardAvoidingView
} from "react-native";
import { Image, Icon, Button, Text, Overlay } from "react-native-elements";
import Toast, { DURATION } from "react-native-easy-toast";

// Acceder camera
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

import t from "tcomb-form-native";
const Form = t.form.Form;
import {
  AddRestaurantStruct,
  AddRestaurantOptions
} from "../../forms/AddRestaurant";

// Upload
import { uploadImage } from "../../utils/UploadImage";
// Firebase app config
import fireBaseApp from "../../utils/FireBase";
// fire base app
import fireBase from "firebase/app";
// FireBase store
import "firebase/firestore";
const db = fireBase.firestore(fireBaseApp);

export default class AddRestaurant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      imageUriRestaurant: "",
      formData: {
        name: "",
        city: "",
        address: "",
        description: ""
      }
    };
  }

  isImageRestaurant = image => {
    if (image) {
      return (
        <Image source={{ uri: image }} style={{ width: 500, height: 200 }} />
      );
    } else {
      return (
        <Image
          source={require("../../../assets/img/restaurante.png")}
          style={{ width: 200, height: 200 }}
        />
      );
    }
  };

  upLoadImageFromGallery = async () => {
    const resultPermissions = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if (resultPermissions.status === "denied") {
      this.refs.toast.show(
        "Es necesario aceptar los permisos de la galeria.",
        1500
      );
    } else {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true
      });

      if (result.cancelled) {
        this.refs.toast.show("Has cerrado la galeria de imagenes.", 1500);
      } else {
        this.setState({
          imageUriRestaurant: result.uri
        });
      }
    }
  };

  onChangeFormAddRestaurant = formValue => {
    this.setState({
      formData: formValue
    });
  };

  addRestaurant = () => {
    const { imageUriRestaurant } = this.state;
    const { name, city, address, description } = this.state.formData;

    if (imageUriRestaurant && name && city && address && description) {
      this.setState({ loading: true });
      // Creando registro
      db.collection("restaurants")
        .add({
          name,
          city,
          address,
          description,
          image: "",
          createAt: new Date(),
          rating: 0,
          ratingTotal: 0,
          quantityVoting: 0
        })
        .then(resolve => {
          const restaurantId = resolve.id;
          //Subiendo imagen
          uploadImage(imageUriRestaurant, restaurantId, "restaurants")
            .then(resolve => {
              const restauranteRef = db
                .collection("restaurants")
                .doc(restaurantId);
              // Actualizando campo image en la coleccion
              restauranteRef
                .update({ image: resolve })
                .then(() => {
                  // OK proceso
                  this.setState({ loading: false });
                  this.refs.toast.show(
                    "Restaurante creado correctamente.",
                    100,
                    () => {
                      this.props.navigation.state.params.loadRestaurants();
                      this.props.navigation.goBack();
                    }
                  );
                })
                .catch(erro => {
                  this.setState({ loading: false });
                  console.log(
                    "Error actualizando imagen en el documento: ",
                    erro
                  );
                  this.refs.toast.show("Error de servidor, intento más tarde.");
                });
            })
            .catch(erro => {
              this.setState({ loading: false });
              console.log("Erro subiendo imagen: ", erro);
              this.refs.toast.show("Error de servidor, intento más tarde.");
            });
        })
        .catch(erro => {
          this.setState({ loading: false });
          console.log("Error guardando registros: ", erro);
          this.refs.toast.show("Error de servidor, intento más tarde.");
        });
    } else {
      this.refs.toast.show("Tienes que rellenar todos los campos.");
    }
  };

  render() {
    const { imageUriRestaurant, loading } = this.state;

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          >
            <View style={styles.viewBody}>
              <View style={styles.viewPhoto}>
                {this.isImageRestaurant(imageUriRestaurant)}
              </View>

              <View>
                <Form
                  ref="addRestaurantForm"
                  type={AddRestaurantStruct}
                  options={AddRestaurantOptions}
                  value={this.state.formData}
                  onChange={formValue =>
                    this.onChangeFormAddRestaurant(formValue)
                  }
                ></Form>
              </View>

              <View style={styles.viewIconUpLoadPhoto}>
                <Icon
                  name="camera"
                  type="material-community"
                  color="#7A7A7A"
                  iconStyle={styles.addPhotoIcon}
                  onPress={() => this.upLoadImageFromGallery()}
                />
              </View>

              <View style={styles.viewButtonAddRestaurant}>
                <Button
                  buttonStyle={styles.buttonAddRestauran}
                  title="Crear restaurante"
                  onPress={() => this.addRestaurant()}
                />
              </View>
            </View>

            <Overlay
              overlayStyle={styles.overlayLoading}
              isVisible={loading}
              width="auto"
              height="auto"
            >
              <View>
                <Text style={styles.overlayTexto}>Creando restaurante</Text>
                <ActivityIndicator
                  size="large"
                  color="#00a680"
                ></ActivityIndicator>
              </View>
            </Overlay>

            <Toast
              ref="toast"
              position="bottom"
              positionValue={200}
              fadeInDuration={1000}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: "#fff" }}
            ></Toast>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1
  },
  viewPhoto: {
    alignItems: "center",
    height: 200,
    marginBottom: 20
  },
  viewIconUpLoadPhoto: {
    flex: 1,
    alignItems: "flex-start",
    marginLeft: 12
  },
  addPhotoIcon: {
    backgroundColor: "#e3e3e3",
    padding: 17,
    paddingBottom: 14,
    margin: 0
  },
  viewButtonAddRestaurant: {
    flex: 1,
    justifyContent: "flex-end"
  },
  buttonAddRestauran: {
    margin: 20,
    backgroundColor: "#00a680"
  },
  overlayLoading: {
    padding: 20
  },
  overlayTexto: {
    color: "#00a680",
    fontSize: 16,
    marginBottom: 20
  }
});
