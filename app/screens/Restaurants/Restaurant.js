import React, { Component } from "react";

import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  SafeAreaView,
  ScrollView,
  FlatList
} from "react-native";

import {
  Image,
  Icon,
  ListItem,
  Button,
  Rating,
  Avatar
} from "react-native-elements";

import Toast, { DURATION } from "react-native-easy-toast";

import { fireBaseApp } from "../../utils/FireBase";
import fireBase from "firebase/app";
import "firebase/firestore";
const db = fireBase.firestore(fireBaseApp);

export default class Restaurant extends Component {
  constructor(props) {
    super(props);

    this.state = {
      reviews: null,
      startReview: null,
      limitReview: 5,
      isLoading: true,
      rating: 0
    };
  }

  componentDidMount = () => {
    this.loadReviews();
  };

  checkUserLogin = () => {
    const user = fireBase.auth().currentUser;
    if (user) {
      return true;
    }
    return false;
  };

  checkAddReviewUser = () => {
    const user = fireBase.auth().currentUser;
    const idUser = user.uid;
    const idRestaurant = this.props.navigation.state.params.restaurant.item
      .restaurant.id;

    const reviews = db.collection("reviews");
    const queryRef = reviews
      .where("id", "==", idUser)
      .where("idRestaurant", "==", idRestaurant);

    return queryRef
      .get()
      .then(resolve => {
        const countReview = resolve.size;
        if (countReview > 0) {
          return true;
        } else {
          return false;
        }
      })
      .catch(erro => {
        console.log("Error consultando reviews", erro);
        throw erro;
      });
  };

  goToScreenAddReview = () => {
    this.checkAddReviewUser()
      .then(resolve => {
        if (resolve) {
          this.refs.toast.show(
            "Ya has enviado una review, no puedes enviar más.",
            2000
          );
        } else {
          const {
            id,
            name
          } = this.props.navigation.state.params.restaurant.item.restaurant;
          this.props.navigation.navigate("AddReviewRestaurant", {
            name,
            id,
            loadReviews: this.loadReviews
          });
        }
      })
      .catch(erro => {
        this.refs.toast.show("Error consultando reviews", 2000);
      });
  };

  loadButtonAddReview = () => {
    if (!this.checkUserLogin()) {
      return (
        <Text>
          Para escribir una review tienes que iniciar sesión, puedes hacerlo{" "}
          <Text
            onPress={() => this.props.navigation.navigate("Login")}
            style={{ color: "#00a680", fontWeight: "bold" }}
          >
            Aquí.
          </Text>
        </Text>
      );
    } else {
      return (
        <Button
          title="Añadir comentario"
          onPress={() => this.goToScreenAddReview()}
          buttonStyle={styles.btnAddReview}
        />
      );
    }
  };

  loadReviews = async () => {
    const { limitReview } = this.state;
    const {
      id
    } = this.props.navigation.state.params.restaurant.item.restaurant;

    let resultReviews = [];

    let arrayRating = [];

    const reviews = db
      .collection("reviews")
      .where("idRestaurant", "==", id)
      .limit(limitReview);

    return await reviews
      .get()
      .then(response => {
        this.setState({
          startReview: response.docs[response.docs.length - 1]
        });

        response.forEach(doc => {
          let review = doc.data();
          resultReviews.push(review);

          arrayRating.push(doc.data().rating);
        });

        // Puntuacion del restaurante
        let numSum = 0;
        arrayRating.map(value => {
          numSum += value;
        });
        const countRating = arrayRating.length;
        const resultRating = countRating > 0 ? numSum / countRating : 0;

        this.setState({
          reviews: resultReviews,
          rating: resultRating.toFixed(2)
        });
      })
      .catch(erro => {
        console.log("Error consultando reviews", erro);
      });
  };

  renderFlatList = reviews => {
    if (reviews) {
      return (
        <FlatList
          data={reviews}
          renderItem={this.renderRow}
          keyExtractor={(item, index) => index.toString()}
          onEndReachedThreshold={0}
        />
      );
    } else {
      return (
        <View style={{ marginTop: 10, alignItems: "center" }}>
          <ActivityIndicator size="large" />
          <Text>Cargando Reviews</Text>
        </View>
      );
    }
  };

  renderRow = reviewData => {
    const {
      title,
      review,
      rating,
      id,
      createAt,
      idRestaurant,
      avatarUser
    } = reviewData.item;

    const createReview = new Date(createAt.seconds * 1000);

    const avatar = avatarUser
      ? avatarUser
      : "https://api.adorable.io/avatars/285/abott@adorable.png";

    return (
      <View
        style={{
          flexDirection: "row",
          margin: 10,
          padding: 10,
          borderBottomColor: "#e3e3e3",
          borderBottomWidth: 1
        }}
      >
        <View style={{ marginRight: 15 }}>
          <Avatar
            source={{
              uri: avatar
            }}
            size="large"
            rounded
            containerStyle={{ width: 50, height: 50 }}
          />
        </View>
        <View style={{ flex: 1, alignItems: "flex-start" }}>
          <Text style={{ fontWeight: "bold" }}>{title}</Text>
          <Text style={{ paddingTop: 2, color: "grey", marginBottom: 5 }}>
            {review}
          </Text>
          <Rating imageSize={15} startingValue={rating} readonly />
          <Text style={{ marginTop: 5, color: "grey", fontSize: 12 }}>
            {createReview.getDate()}/{createReview.getMonth() + 1}/
            {createReview.getFullYear()} - {createReview.getHours()}:
            {createReview.getMinutes()}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    const { reviews, rating } = this.state;

    const {
      id,
      name,
      city,
      address,
      description,
      image
    } = this.props.navigation.state.params.restaurant.item.restaurant;

    const listExtraInfo = [
      {
        text: `${city}, ${address}`,
        iconName: "map-marker",
        iconType: "material-community",
        action: null
      }
    ];

    return (
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={styles.viewBody}
      >
        <View style={styles.viewImage}>
          <Image
            source={{ uri: image }}
            PlaceholderContent={<ActivityIndicator></ActivityIndicator>}
            style={styles.imageRestaurant}
          ></Image>
        </View>

        <View style={styles.viewRestaurantBasicInfo}>
          <View>
            <Text style={styles.nameRestaurant}>{name}</Text>
          </View>

          <Text style={styles.descriptionRestaurant}>{description}</Text>
        </View>

        <View style={styles.viewRestaurantExtraInfo}>
          <Text style={styles.restaurantExtraInfoTitle}>
            Información sobre el restaurante
          </Text>
          {listExtraInfo.map((item, index) => (
            <ListItem
              key={index}
              title={item.text}
              leftIcon={<Icon type={item.iconType} name={item.iconName}></Icon>}
            ></ListItem>
          ))}

          <View style={{ flex: 1, alignItems: "flex-start" }}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                marginBottom: 10,
                marginTop: 10
              }}
            >
              Calificación
            </Text>
            <Rating
              imageSize={20}
              readonly
              startingValue={parseFloat(rating)}
            />
          </View>
        </View>

        <View style={styles.viewBtnAddReview}>
          {this.loadButtonAddReview()}
        </View>

        <Text
          style={{
            fontSize: 20,
            textAlign: "center",
            marginTop: 10,
            fontWeight: "bold",
            marginBottom: 10
          }}
        >
          Comentarios
        </Text>

        {this.renderFlatList(reviews)}

        <Toast
          ref="toast"
          position="bottom"
          positionValue={250}
          fadeInDuration={1000}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: "#fff" }}
        ></Toast>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1
  },
  viewImage: {
    width: "100%"
  },
  imageRestaurant: {
    width: "100%",
    height: 200,
    resizeMode: "cover"
  },
  viewRestaurantBasicInfo: {
    margin: 15
  },
  nameRestaurant: {
    fontSize: 20,
    fontWeight: "bold"
  },
  descriptionRestaurant: {
    marginTop: 5,
    color: "grey"
  },
  viewRestaurantExtraInfo: {
    marginTop: 25,
    margin: 15
  },
  restaurantExtraInfoTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10
  },
  viewBtnAddReview: {
    margin: 20
  },
  btnAddReview: {
    backgroundColor: "#00a680"
  }
});
