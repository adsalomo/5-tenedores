import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import ActionButton from "react-native-action-button";
import { Image } from "react-native-elements";

import { fireBaseApp } from "../../utils/FireBase";
import fireBase from "firebase/app";
import "firebase/firestore";
const db = fireBase.firestore(fireBaseApp);

export default class Restaurants extends Component {
  constructor() {
    super();
    this.state = {
      login: false,
      restaurants: null,
      startRestaurants: null,
      limitRestaurants: 8,
      isLoading: true
    };
  }

  componentDidMount = () => {
    this.checkLogin();
    this.loadRestaurants();
  };

  checkLogin = () => {
    fireBase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({
          login: true
        });
      } else {
        this.setState({
          login: false
        });
      }
    });
  };

  loadRestaurants = async () => {
    const { limitRestaurants } = this.state;
    let resultRestaurants = [];
    const restaurants = db
      .collection("restaurants")
      .orderBy("createAt", "desc")
      .limit(limitRestaurants);

    await restaurants.get().then(response => {
      this.setState({
        startRestaurants: response.docs[response.docs.length - 1]
      });

      response.forEach(doc => {
        let restaurant = doc.data();
        restaurant.id = doc.id;
        resultRestaurants.push({ restaurant });
      });
    });

    this.setState({
      restaurants: resultRestaurants
    });
  };

  loadActionButton = () => {
    const { login } = this.state;
    if (login) {
      return (
        <ActionButton
          buttonColor="#00a680"
          onPress={() => {
            this.goToScreen("AddRestaurant");
          }}
        />
      );
    }
    return null;
  };

  goToScreen = nameScreen => {
    this.props.navigation.navigate(nameScreen, {
      loadRestaurants: this.loadRestaurants
    });
  };

  clickRestaurant = restaurant => {
    this.props.navigation.navigate("Restaurant", { restaurant });
  };

  renderRow = restaurant => {
    const {
      name,
      city,
      description,
      image,
      address
    } = restaurant.item.restaurant;

    return (
      <TouchableOpacity onPress={() => this.clickRestaurant(restaurant)}>
        <View style={styles.viewRestaurant}>
          <View style={styles.viewRestaurantImage}>
            <Image
              resizeMode="cover"
              source={{ uri: image }}
              style={styles.imageRestaurant}
            ></Image>
          </View>
          <View>
            <Text style={styles.flatListRestaurantName}>{name}</Text>
            <Text style={styles.flatListRestaurantAddress}>
              {city}, {address}
            </Text>
            <Text style={styles.flatListRestaurantDescription}>
              {description.substr(0, 60)}...
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  handleLoadMore = async () => {
    const { limitRestaurants, startRestaurants } = this.state;

    let resultRestaurants = [];

    this.state.restaurants.forEach(doc => {
      resultRestaurants.push(doc);
    });

    const restaurantsDb = db
      .collection("restaurants")
      .orderBy("createAt", "desc")
      .startAfter(startRestaurants.data().createAt)
      .limit(limitRestaurants);

    await restaurantsDb.get().then(response => {
      if (response.docs.length > 0) {
        this.setState({
          startRestaurants: response.docs[response.docs.length - 1]
        });
      } else {
        this.setState({
          isLoading: false
        });
      }

      response.forEach(doc => {
        let restaurant = doc.data();
        restaurant.id = doc.id;
        resultRestaurants.push({ restaurant });
      });

      this.setState({
        restaurants: resultRestaurants
      });
    });
  };

  renderFooter = () => {
    if (this.state.isLoading) {
      return (
        <View style={styles.loaderRestaurants}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    } else {
      return (
        <View style={styles.noFoundRestaurants}>
          <Text>No quedan restaurantes por cargar</Text>
        </View>
      );
    }
  };

  renderFlatList = restaurants => {
    if (restaurants) {
      return (
        <FlatList
          data={this.state.restaurants}
          renderItem={this.renderRow}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.5}
          ListFooterComponent={this.renderFooter}
        ></FlatList>
      );
    } else {
      return (
        <View style={styles.startLoadRestaurants}>
          <ActivityIndicator size="large" />
          <Text>Cargando restaurantes</Text>
        </View>
      );
    }
  };

  render() {
    const { restaurants } = this.state;
    return (
      <View style={styles.viewBody}>
        {this.renderFlatList(restaurants)}
        {this.loadActionButton()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1
  },
  startLoadRestaurants: {
    marginTop: 20,
    alignItems: "center"
  },
  viewRestaurant: {
    flexDirection: "row",
    margin: 10
  },
  imageRestaurant: {
    width: 80,
    height: 80
  },
  viewRestaurantImage: {
    marginRight: 15
  },
  flatListRestaurantName: {
    fontWeight: "bold"
  },
  flatListRestaurantAddress: {
    paddingTop: 2,
    color: "grey"
  },
  flatListRestaurantDescription: {
    paddingTop: 2,
    color: "grey",
    width: 250
  },
  loaderRestaurants: {
    marginTop: 10,
    marginBottom: 10
  },
  noFoundRestaurants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center"
  }
});
