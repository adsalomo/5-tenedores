import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import { SearchBar, ListItem, Icon } from "react-native-elements";

import { fireBaseApp } from "../utils/FireBase";
import fireBase from "firebase/app";
import "firebase/firestore";
const db = fireBase.firestore(fireBaseApp);

import { FireSQL } from "firesql";
const fireSQL = new FireSQL(fireBase.firestore(), { includeId: "id" });

export default class Search extends Component {
  constructor() {
    super();
    this.state = {
      search: ""
    };
  }

  searchRestaurants = async value => {
    this.setState({
      search: value
    });

    let resultRestaurants = null;

    const restaurants = fireSQL.query(
      "SELECT * FROM restaurants WHERE name LIKE '" + value + "%'"
    );

    await restaurants
      .then(response => {
        resultRestaurants = response;
      })
      .catch(erro => {});

    this.setState({
      restaurants: resultRestaurants
    });
  };

  renderListRestaurant = restaurants => {
    if (restaurants) {
      return (
        <View>
          {restaurants.map((restaurant, index) => {
            let restauranteClick = {
              item: {
                restaurant: null
              }
            };
            restauranteClick.item.restaurant = restaurant;

            return (
              <ListItem
                key={index}
                title={restaurant.name}
                leftAvatar={{ source: { uri: restaurant.image } }}
                rightIcon={
                  <Icon type="material-community" name="chevron-right" />
                }
                onPress={() => this.clickRestaurant(restauranteClick)}
              />
            );
          })}
        </View>
      );
    } else {
      return (
        <View>
          <Text style={{ textAlign: "center" }}>Busca tus restaurantes</Text>
        </View>
      );
    }
  };

  clickRestaurant = restaurant => {
    this.props.navigation.navigate("Restaurant", { restaurant });
  };

  render() {
    const { search, restaurants } = this.state;
    return (
      <View style={styles.viewBody}>
        <SearchBar
          placeholder="Buscar restaurantes"
          onChangeText={this.searchRestaurants}
          value={search}
          containerStyle={{ marginBottom: 20, backgroundColor: "#fff" }}
          lightTheme={true}
        />
        {this.renderListRestaurant(restaurants)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1
  }
});
