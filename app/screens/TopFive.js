import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity
} from "react-native";

import { Card, Image, Rating } from "react-native-elements";

import { fireBaseApp } from "../utils/FireBase";
import fireBase from "firebase/app";
import "firebase/firestore";
const db = fireBase.firestore(fireBaseApp);

export default class TopFive extends Component {
  constructor() {
    super();

    this.state = {
      restaurants: null
    };
  }

  componentDidMount = () => {
    this.loadTopFiveRestaurant();
  };

  loadTopFiveRestaurant = async () => {
    const restaurants = db
      .collection("restaurants")
      .orderBy("rating", "desc")
      .limit(5);

    let restaurantsArray = [];
    await restaurants
      .get()
      .then(response => {
        response.forEach(doc => {
          let restaurant = doc.data();
          restaurant.id = doc.id;
          restaurantsArray.push(restaurant);
        });
      })
      .catch(erro => {
        console.log("Error trayendo restaurantes", erro);
      });

    this.setState({
      restaurants: restaurantsArray
    });
  };

  clickRestaurant = restaurant => {
    this.props.navigation.navigate("Restaurant", { restaurant });
  };

  renderRestaurants = restaurants => {
    if (restaurants) {
      return (
        <View>
          {restaurants.map((restaurant, index) => {
            let restaurantClick = {
              item: {
                restaurant: restaurant
              }
            };
            return (
              <TouchableOpacity
                key={index}
                onPress={() => this.clickRestaurant(restaurantClick)}
              >
                <Card>
                  <Image
                    style={{ width: "100%", height: 200 }}
                    resizeMode="cover"
                    source={{ uri: restaurant.image }}
                  />
                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      {restaurant.name}
                    </Text>
                    <Rating
                      imageSize={20}
                      startingValue={restaurant.rating}
                      readonly
                      style={{ position: "absolute", right: 0 }}
                    />
                  </View>
                  <Text
                    style={{
                      color: "grey",
                      marginTop: 10,
                      textAlign: "justify"
                    }}
                  >
                    {restaurant.description}
                  </Text>
                </Card>
              </TouchableOpacity>
            );
          })}
        </View>
      );
    } else {
      return (
        <View style={{ alignItems: "center" }}>
          <ActivityIndicator size="large" />
          <Text>Cargando restaurantes</Text>
        </View>
      );
    }
  };

  render() {
    const { restaurants } = this.state;
    return (
      <ScrollView style={styles.viewBody}>
        {this.renderRestaurants(restaurants)}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1
  }
});
