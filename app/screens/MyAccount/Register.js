import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ActivityIndicator,
  KeyboardAvoidingView,
  ScrollView
} from "react-native";
import { Button, Text, Image } from "react-native-elements";
import Toast, { DURATION } from "react-native-easy-toast";

// Formulario
import t from "tcomb-form-native";
const Form = t.form.Form;
import { RegisterScruct, RegisterOptions } from "../../forms/Register";

// Firebase
import * as fireBase from "firebase";

export default class Register extends Component {
  constructor() {
    super();
    this.state = {
      registerScruct: RegisterScruct,
      registerOptions: RegisterOptions,
      formData: {
        name: "",
        email: "",
        password: "",
        passwordConfirmation: ""
      },
      formErrorMessage: ""
    };
  }

  register = () => {
    const { password, passwordConfirmation } = this.state.formData;
    if (password === passwordConfirmation) {
      const validate = this.refs.registerForm.getValue();
      if (validate) {
        this.setState({
          formErrorMessage: ""
        });
        fireBase
          .auth()
          .createUserWithEmailAndPassword(validate.email, validate.password)
          .then(resolve => {
            this.refs.toast.show("Registro correcto.", 200, () => {
              //this.props.navigation.navigate("MyAccount");
              this.props.navigation.goBack();
            });
          })
          .catch(err => {
            this.refs.toast.show("El email ya esta en uso.", 2500);
          });
      } else {
        this.setState({
          formErrorMessage: "Formulario inválido"
        });
      }
    } else {
      this.setState({
        formErrorMessage: "Las contraseñas no son iguales"
      });
    }
  };

  onChangeFormRegister = formValue => {
    this.setState({
      formData: formValue
    });
  };

  render() {
    const { registerScruct, registerOptions, formErrorMessage } = this.state;

    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.viewBody}>
            <Image
              source={require("../../../assets/img/5-tenedores-letras-icono-logo.png")}
              style={styles.logo}
              PlaceholderContent={<ActivityIndicator />}
              resizeMode="contain"
            />
            <Form
              ref="registerForm"
              type={registerScruct}
              options={registerOptions}
              value={this.state.formData}
              onChange={formValue => this.onChangeFormRegister(formValue)}
            />
            <Button
              buttonStyle={styles.buttonRegisterContainer}
              title="Unirse"
              onPress={() => this.register()}
            ></Button>
            <Text style={styles.formErrorMessage}>{formErrorMessage}</Text>
            <Toast
              ref="toast"
              position="bottom"
              positionValue={250}
              fadeInDuration={1000}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: "#fff" }}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    justifyContent: "center",
    marginLeft: 40,
    marginRight: 40,
    marginTop: 10
  },
  buttonRegisterContainer: {
    backgroundColor: "#00a680",
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10
  },
  formErrorMessage: {
    color: "#f00",
    textAlign: "center",
    marginTop: 10
  },
  logo: {
    marginLeft: 40,
    marginRight: 40,
    height: 150,
    marginBottom: 20
  }
});
