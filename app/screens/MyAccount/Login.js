import React, { Component } from "react";
SafeAreaView;
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import { Image, Button, SocialIcon, Divider } from "react-native-elements";
import Toast, { DURATION } from "react-native-easy-toast";
import * as Facebook from "expo-facebook";

import t from "tcomb-form-native";
const Form = t.form.Form;
import { LoginStruct, LoginOptions } from "../../forms/Login";
import * as fireBase from "firebase";
import { FacebookApi } from "../../utils/Social";

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      loginStruct: LoginStruct,
      loginOptions: LoginOptions,
      loginData: {
        email: "",
        password: ""
      },
      loginErrorMessage: ""
    };
  }

  login = () => {
    const validate = this.refs.loginForm.getValue();
    if (!validate) {
      this.setState({
        loginErrorMessage: "Formulario inválido"
      });
    } else {
      this.setState({
        loginErrorMessage: ""
      });
      fireBase
        .auth()
        .signInWithEmailAndPassword(validate.email, validate.password)
        .then(() => {
          this.refs.toastLogin.show("Registro correcto.", 200, () => {
            this.props.navigation.goBack();
          });
        })
        .catch(erro => {
          this.refs.toastLogin.show("Login incorrecto.", 2500);
        });
    }
  };

  onChangeFormLogin = formValue => {
    this.setState({
      loginData: formValue
    });
  };

  loginFacebook = async () => {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions
      } = await Facebook.logInWithReadPermissionsAsync(
        FacebookApi.application_id,
        {
          permissions: FacebookApi.permissions
        }
      );

      if (type === "success") {
        const credentials = fireBase.auth.FacebookAuthProvider.credential(
          token
        );
        fireBase
          .auth()
          .signInWithCredential(credentials)
          .then(() => {
            this.refs.toastLogin.show("Registro correcto.", 100, () => {
              this.props.navigation.goBack();
            });
          })
          .catch(erro => {
            this.refs.toastLogin.show("Login incorrecto.", 2500);
          });
      } else if (type === "cancel") {
        this.refs.toastLogin.show("Inicio de Sesión Cancelado.", 300);
      } else {
        this.refs.toastLogin.show(
          "Login incorrecto, por favor vuelva a intentarlo.",
          300
        );
      }
    } catch ({ message }) {
      this.refs.toastLogin.show("Login incorrecto con Facebook.", 2500);
    }
  };

  render() {
    const { loginStruct, loginOptions, loginErrorMessage } = this.state;
    let espacio = " ";

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          >
            <View style={styles.viewBody}>
              <Image
                source={require("../../../assets/img/5-tenedores-letras-icono-logo.png")}
                style={styles.logo}
                PlaceholderContent={<ActivityIndicator />}
                resizeMode="contain"
              />
              <View style={styles.viewForm}>
                <Form
                  ref="loginForm"
                  type={loginStruct}
                  options={loginOptions}
                  value={this.state.loginData}
                  onChange={formValue => this.onChangeFormLogin(formValue)}
                />

                <Button
                  buttonStyle={styles.buttonLoginContainer}
                  title="Login"
                  onPress={() => this.login()}
                />

                <Text style={styles.textRegister}>
                  ¿Aún no tienes una cuenta?
                  {espacio}
                  <Text
                    style={styles.btnRegister}
                    onPress={() => this.props.navigation.navigate("Register")}
                  >
                    Registrate
                  </Text>
                </Text>

                <Text style={styles.loginErrorMessage}>
                  {loginErrorMessage}
                </Text>

                <Divider style={styles.divider}></Divider>

                <SocialIcon
                  title="Iniciar Sesión con Facebook"
                  button
                  type="facebook"
                  onPress={() => this.loginFacebook()}
                />
              </View>

              <Toast
                ref="toastLogin"
                position="bottom"
                positionValue={250}
                fadeInDuration={1000}
                fadeOutDuration={1000}
                opacity={0.8}
                textStyle={{ color: "#fff" }}
              ></Toast>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    marginBottom: 10
  },
  textRegister: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    textAlign: "center"
  },
  logo: {
    marginLeft: 40,
    marginRight: 40,
    height: 150
  },
  viewForm: {
    marginTop: 30
  },
  buttonLoginContainer: {
    backgroundColor: "#00a680",
    marginTop: 20,
    marginRight: 10,
    marginLeft: 10
  },
  loginErrorMessage: {
    color: "#f00",
    textAlign: "center",
    marginTop: 10,
    marginBottom: 10
  },
  divider: {
    backgroundColor: "#00a680",
    marginBottom: 10
  },
  btnRegister: {
    color: "#00a680",
    fontWeight: "bold"
  }
});
