import React, { Component } from "react";
import { StyleSheet, View, Text, ActivityIndicator } from "react-native";
import { Button, Image } from "react-native-elements";

import UserInfo from "./UserInfo";

// No es una screens, no se pueden usar los props para navegar
export default class MyAccountUser extends Component {
  render() {
    return (
      <View style={styles.viewUserAccount}>
        <UserInfo></UserInfo>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewUserAccount: {
    height: "100%",
    backgroundColor: "#f2f2f2"
  }
});
