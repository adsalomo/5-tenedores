import React, { Component } from "react";
import { StyleSheet, View, Text, ActivityIndicator } from "react-native";
import { Avatar, Button } from "react-native-elements";
import Toast, { DURATION } from "react-native-easy-toast";

import UpdateUserInfo from "./UpdateUserInfo";

import * as fireBase from "firebase";

// Acceder camera
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

export default class UserInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ...props,
      userInfo: {
        displayName: "",
        email: "",
        photoURL: ""
      }
    };
  }

  componentDidMount = async () => {
    await this.getUserInfo();
  };

  getUserInfo = () => {
    const user = fireBase.auth().currentUser;

    user.providerData.forEach(userInfo => {
      this.setState({
        userInfo: userInfo
      });
    });
  };

  reauthenticate = currentPassword => {
    const user = fireBase.auth().currentUser;
    const credentials = fireBase.auth.EmailAuthProvider.credential(
      user.email,
      currentPassword
    );
    return user.reauthenticateWithCredential(credentials);
  };

  updateUserDisplayName = async newDisplayName => {
    const update = {
      displayName: newDisplayName
    };
    await fireBase.auth().currentUser.updateProfile(update);
    this.getUserInfo();
  };

  updateUserEmail = async (newEmail, password) => {
    this.reauthenticate(password)
      .then(() => {
        const user = fireBase.auth().currentUser;
        user
          .updateEmail(newEmail)
          .then(() => {
            this.refs.toastLogin.show(
              "Email se cambió correctamente. Por favor vuelve a iniciar Sesión.",
              200,
              () => {
                fireBase.auth().signOut();
              }
            );
          })
          .catch(erro => {
            this.refs.toastLogin.show("Error cambiando email.", 300);
          });
      })
      .catch(erro => {
        this.refs.toastLogin.show("Error reautenticar.", 300);
      });
  };

  updateUserPassword = async (currentPassword, newPassword) => {
    this.reauthenticate(currentPassword)
      .then(() => {
        const user = fireBase.auth().currentUser;
        user
          .updatePassword(newPassword)
          .then(() => {
            this.refs.toastLogin.show(
              "Contraseña actualizada correctamente. Por favor vuelve a iniciar Sesión.",
              200,
              () => {
                fireBase.auth().signOut();
              }
            );
          })
          .catch(erro => {
            this.refs.toastLogin.show(
              "Error del servidor, intentelo más tarde.",
              1500
            );
          });
      })
      .catch(erro => {
        this.refs.toastLogin.show(
          "Tu contraseña actual introducia no es correcta.",
          1500
        );
      });
  };

  checkUserAvatar = photoURL => {
    return photoURL
      ? photoURL
      : "https://api.adorable.io/avatars/285/abott@adorable.png";
  };

  returnUpdateUserInfoComponent = userInfo => {
    if (userInfo.hasOwnProperty("uid")) {
      return (
        <UpdateUserInfo
          userInfo={this.state.userInfo}
          updateUserDisplayName={this.updateUserDisplayName}
          updateUserEmail={this.updateUserEmail}
          updateUserPassword={this.updateUserPassword}
        />
      );
    }
  };

  changeAvatarUser = async () => {
    const resultPermissions = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );

    if (resultPermissions.status === "denied") {
      this.refs.toastLogin.show(
        "Es necesario aceptar los permisos de la galeria.",
        1500
      );
    } else {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3]
      });

      if (result.cancelled) {
        this.refs.toastLogin.show("Has cerrado la galeria de imagenes.", 1500);
      } else {
        const { uid } = this.state.userInfo;
        this.uploadImage(result.uri, uid)
          .then(resolve => {
            this.refs.toastLogin.show(
              "Avatar actualizado correctamente.",
              1500
            );
            fireBase
              .storage()
              .ref("avatars/" + uid)
              .getDownloadURL()
              .then(resolve => {
                this.updateUserPhotoUrl(resolve);
              })
              .catch(erro => {
                this.refs.toastLogin.show(
                  "Error al recuperar el avatar del servidor.",
                  1500
                );
              });
          })
          .catch(erro => {
            this.refs.toastLogin.show(
              "Error al actualizar el avatar, intentelo más tade.",
              1500
            );
          });
      }
    }
  };

  uploadImage = async (uri, nameImage) => {
    await fetch(uri)
      .then(async result => {
        elStorage = fireBase
          .storage()
          .ref()
          .child("avatars/" + nameImage);
        return await elStorage.put(result._bodyBlob);
      })
      .catch(error => {
        this.refs.toastLogin.show(
          "Ocurrio un error, inténtalo nuevamente.",
          1500
        );
      });
  };

  updateUserPhotoUrl = async photoUrl => {
    const update = {
      photoURL: photoUrl
    };
    await fireBase.auth().currentUser.updateProfile(update);
    this.getUserInfo();
  };

  render() {
    const { displayName, email, photoURL } = this.state.userInfo;
    return (
      <View>
        <View style={styles.viewUser}>
          <Avatar
            rounded
            size="large"
            showEditButton
            source={{ uri: this.checkUserAvatar(photoURL) }}
            containerStyle={styles.userInfoAvatar}
            onEditPress={() => this.changeAvatarUser()}
          ></Avatar>
          <View>
            <Text style={styles.displayName}>{displayName}</Text>
            <Text>{email}</Text>
          </View>
        </View>
        {this.returnUpdateUserInfoComponent(this.state.userInfo)}
        <Button
          title="Cerrar Sesión"
          onPress={() => fireBase.auth().signOut()}
          buttonStyle={styles.btnCloseSession}
          titleStyle={styles.btnCloseSessionText}
        ></Button>
        <Toast
          ref="toastLogin"
          position="bottom"
          positionValue={250}
          fadeInDuration={1000}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: "#fff" }}
        ></Toast>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewUser: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    paddingTop: 30,
    paddingBottom: 30,
    backgroundColor: "#f2f2f2"
  },
  userInfoAvatar: {
    marginRight: 20
  },
  displayName: {
    fontWeight: "bold"
  },
  btnCloseSession: {
    marginTop: 30,
    borderRadius: 0,
    backgroundColor: "#fff",
    borderTopWidth: 1,
    borderTopColor: "#e3e3e3",
    borderBottomColor: "#e3e3e3",
    borderBottomWidth: 1,
    paddingTop: 10,
    paddingBottom: 10
  },
  btnCloseSessionText: {
    color: "#00a680"
  }
});
